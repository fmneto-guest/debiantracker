# Debian Tracker

This bot is a python script that makes a JSON request to the UDD (Ultimade Debian Database) to determine how many Release-Critical bugs affecting the current Testing release are currently open. It then composes a tweet and publishes it using the Tweepy class.

It serves as a way of tracking the progress of the Freeze process towards a new Stable Release.

Currently, it's tracking Debian Bullseye.
