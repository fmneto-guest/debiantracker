#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright (c) 2019 Francisco M Neto
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import tweepy
import re
import requests
import datetime as dt
from dateutil.relativedelta import relativedelta, MO
import json
import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.dates import SU
from scipy import optimize
from scipy.optimize import fsolve
from secrets import *
from time import gmtime, strftime

path = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

# ====== Individual bot configuration ==========================
bot_username = 'debian_tracker'
logfile_name = os.path.join(path, bot_username + ".log")
plotfile_name = os.path.join(path, 'rcbugs.log')
sevfile_name = os.path.join(path, 'bug_severity.log')
plotimg_file = os.path.join(path, 'bugs.png')
tweet_hour = 15
stable_name = "Buster"
testing_name = "Bullseye"
# ==============================================================

day_th = {
    1: "st",
    2: "nd",
    3: "rd",
    4: "th",
    5: "th",
    6: "th",
    7: "th",
    8: "th",
    9: "th",
    0: "th"
}

# Transpose the input array, so data for a certain time are all in the same row
data = np.transpose(np.loadtxt(sevfile_name, delimiter='\t', unpack= True))

date = [np.datetime64(dt.datetime.utcfromtimestamp(d)) for d in data[:,0]]
bugs = data[:,1:5]

params = []
params_cov = []

def func(x, a, b, c):
    return a*x**2 + b*x + c

def f_func(x):
    return func(x, params[0], params[1], params[2])

def fit_bugs(N):
    global date
    global bugs
    global params
    global params_cov

    p0 = [1.0e0, -9e-6, 1.8e2]

    params, params_cov = optimize.curve_fit(func, date, bugs[:,0], p0)

    resid = 0
    for i in range(N):
        resid += (bugs[i][0] - f_func(bugs[i][0]))**2
    resid /= N

    d_day = fsolve(f_func, 1596815349)
    d_day = fit_bugs(N)

    blurb = "Projected release date: " + str(dt.datetime.fromtimestamp(d_day).strftime("%d/%m/%Y"))
    blurb += "\n"
    blurb += "Planned release date: 06/07/2019"

    return blurb


def plot_bugs(sevs, fit=True):
    ## Fit a function to the data
    ## to "estimate" a release date

    # Reference the global variables
    global date
    global bugs

    ## Define plot legend
    plot_title = "# of RC bugs"
    fit_title = "Fitted curve"

    # Update arrays containing bug data
    time_now = np.datetime64(dt.datetime.now())
    date = np.append(date, time_now)
    bugs = np.vstack((bugs, np.hstack((sum(sevs),sevs))))

    N = len(date)

    if(fit == True):
        blurb = fit_bugs(N)
    else:
        blurb = ""


    ## Set up plot information
    ## like labels, ticks, etc

    # Set graph limits to one week before and one after the range of dates
    min_date = date[0] - np.timedelta64(7, 'D')
    max_date = date[-1] + np.timedelta64(7, 'D')
    min_bugs = np.amin(data, axis=0)[1]
    max_bugs = np.amax(data, axis=0)[1]

    plt.figure(figsize=(12.8, 9.6))
    plt.rcParams.update({'font.size': 14})
    # Plot axes. Show sundays on the X axis, day/month.
    plt.gca().xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=SU))
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%d/%m'))
    plt.xlabel('Date')
    plt.ylabel('# of bugs')
    plt.title(plot_title)
    plt.text(data[math.floor(N/100)][0], math.floor(min_bugs + (max_bugs-min_bugs)/5), blurb,
            bbox=dict(boxstyle="round",
            ec=(.8, .8, .8),
            fc=(1., 1., 1., .7)))
    plt.gca().set_xlim(min_date, max_date)
    plt.grid(True, which='major', axis='both')
    plt.plot(date, bugs[:, 1], '--bo',label="Serious")
    plt.plot(date, bugs[:, 2], '--ro',label="Grave")
    plt.plot(date, bugs[:, 3], '--go',label="Critical")
    if(fit == True):
        plt.plot(date, f_func(bugs[:,0]), color='red', label=fit_title)
    plt.legend()
    print("Saving file " + plotimg_file)
    plt.savefig(plotimg_file)
    print("Showing plot...")
    plt.show()

def fetch_json():
    """Retrieve the number of RC bugs on Testing from UDD."""

    print("Fetching JSON data from UDD...", end="", flush=True)

    URL = "https://udd.debian.org/bugs/?release=bullseye&fnewerval=7&flastmodval=7&rc=1&sortby=id&sorto=desc&format=json#results"
    try:
        data = requests.get(URL).json()
    except:
        print("Decode error, possibly UDD is under heavy load.")
        exit(0)

    print("done.")

    return data

def create_tweet(n_bugs):
    """Create the text of the tweet you want to send."""

    print("Composing tweet...", end="", flush="True")

    stable = dt.datetime(2019,7,9)
    freeze = dt.datetime(2021,1,12)
    planned = dt.datetime(2019,7,6)
    today = dt.datetime.now()

    # Has the freeze begun?
    t_days = freeze - today 

    if(int(t_days.days) > 0):
        extra = ". The Freeze has not begun yet."
    elif(int(t_days.days)%3):
        d = relativedelta(planned, today)
        if(d.days == 1):
            extra = ". The Release is planned for tomorrow! Are you ready?"
        else:    
            extra =  ". " + str(d.days) + " days "
            extra += "until the planned date for " + testing_name + "'s release. Plan your party!"
    elif(int(t_days.days)%2):
        d = relativedelta(today, stable)
        year = str(d.years) + " year " if d.years==1 else str(d.years) + " years "
        month = str(d.months) + " month " if d.months==1 else str(d.months) + " months "
        day = str(d.days) + " day " if d.days==1 else str(d.days) + " days "
        extra = " (" + year + month + day + "since the release of " + stable_name + ")."
    else:
        d = relativedelta(today, freeze)
        if(d.years > 0):
            year = str(d.years) + " year " if d.years==1 else str(d.years) + " years "
        if(d.months > 0):
            month = str(d.months) + " month " if d.months==1 else str(d.months) + " months "
        day = str(d.days) + " day " if d.days==1 else str(d.days) + " days "
        extra = " (" + str(t_days.days) + str(day_th.get(d.days%10, "th")) + " day of the freeze)"

    print("done.")

    return str(n_bugs) + " packages with RC bugs in Testing as of today" + extra

def bug_sevs(data):
    sevs = [0, 0, 0]

    print("Counting bugs by severity...", end="", flush="True")

    for bug in data:
        if(bug['severity'] == 'serious'):
            sevs[0] += 1
        elif(bug['severity'] == 'grave'):
            sevs[1] += 1
        else:
            sevs[2] += 1

    print("done.")

    return sevs


def tweet(sevs, mode):
    """Send out the text as a tweet."""

    # Mode defines if the tweet is sent or printet to output or both.
    # Mode = 1: tweet
    # Mode = 2: print out
    # Mode = 3: both
    # Mode = 0: none

    text = create_tweet(sum(sevs))

    hour = int(dt.datetime.now().strftime("%H"))

    if((mode == 1 or mode == 3) and hour == tweet_hour):
        # Twitter authentication
        print("Preparing connection to Twitter...")
        auth = tweepy.OAuthHandler(C_KEY, C_SECRET)
        auth.set_access_token(A_TOKEN, A_TOKEN_SECRET)
        api = tweepy.API(auth)

    # Send the tweet and log success or failure
    try:
        if(mode == 1 or mode == 3):
            if(hour == tweet_hour):
                print("Sending tweet...")
                api.update_with_media(plotimg_file, text)
                # api.update_status(text)
        elif(mode == 2 or mode == 3):
            print(text)     # Update this when posting to production
        else:
            print("No message going out. Something wrong?")
    except tweepy.error.TweepError as e:
        log(e.reason)
    else:
        log("Tweeted: \"" + text + "\"")
        log_g(sevs)

    print("All done.")


def log(message):
    """Log message to logfile."""
    with open(logfile_name, 'a+') as f:
        t = strftime("%d %b %Y %H:%M:%S", gmtime())
        msg = str(t) + " " + str(message) + "\n"
        f.write(msg)

def log_g(sevs):
    """Log data for later plotting."""
    with open(plotfile_name, 'a+') as g:
        t = dt.datetime.now().timestamp()
        msg = str(t) + "\t" + str(sum(sevs)) + "\n"
        g.write(msg)
        # print(msg)
    with open(sevfile_name, 'a+') as h:
        msg = str(t) + "\t" + str(sum(sevs)) + "\t" + '\t'.join(map(str,sevs)) + "\n"
        h.write(msg)


def run_bot():
    data = fetch_json()
    num = len(data)
    bug_severity = bug_sevs(data)
    plot_bugs(bug_severity, False)
    tweet(bug_severity, 2)

if __name__ == "__main__":
    run_bot()