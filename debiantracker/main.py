#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright (c) 2019-2021 Francisco M Neto
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import datetime as dt
import json
import math
import time
from ftplib import FTP
from secrets import *
from time import gmtime, strftime

import mastodon
import matplotlib.dates as mdt
import matplotlib.pyplot as plt
import numpy as np
import requests
import tweepy
from dateutil.relativedelta import relativedelta
from matplotlib.dates import SU

from debiantracker import db
from debiantracker.model import Bugs, Plot, Updates


class DebianTracker():

    def __init__(self):

        self.bot_username = 'debian_tracker'
        self.mastodon_url = 'https://fosstodon.org'

        self.ftp_host = 'ftp.fmneto.com'
        self.ftp_user = 'dtftp@dt.fmneto.com'
        self.ftp_imgdir = 'img'

        self.stable = ["Bullseye",
                dt.datetime(2021,8,14)]   # RELEAST date
        self.testing = ["Bookworm",
                dt.datetime(2023,1,12)]   # FREEZE date
        self.launch_date = [False,        # Has a launch date been posted?
                False,                    # Is it expected (True) or "tentative" (False)
                dt.datetime(2021,8,14)        # Actual date
                ]

        self.image_prefix = "static/img/"
        self.plot_filename = "bugs.png"
        self.freeze_filename = "freeze.png"

        self.day_th = {
            1: "st",    
            2: "nd",
            3: "rd",
            4: "th",
            5: "th",
            6: "th",
            7: "th",
            8: "th",
            9: "th",
            0: "th"
        }

    def fetch_bug_data(self):
        """Retrieve the number of RC bugs on Testing from UDD."""

        print("Fetching JSON data from UDD...", end="", flush=True)

        URL = "https://udd.debian.org/bugs/?release=bookworm&fnewerval=7&flastmodval=7&rc=1&sortby=id&sorto=desc&format=json#results"
        try:
            data = requests.get(URL).json()
        except:
            print("Decode error, possibly UDD is under heavy load.")
            return None

        print("done.")

        return data

    def save_bug_data(self, sevs):
        '''Save latest bug data into database'''

        now = dt.datetime.now()
        hour = now.hour

        if(hour%6 == 0):
            new_entry = Bugs(
                tstamp = dt.datetime.timestamp(now),
                serious = sevs[0],
                grave = sevs[1],
                critical = sevs[2]
            )

            db.session.add(new_entry)
            db.session.commit()

        return

    def get_latest(self):

        return Bugs.query.order_by(Bugs.tstamp.desc()).first().row()

    def process_bug_sevs(self, data):

        sevs = [0, 0, 0]

        print("Counting bugs by severity...", end="", flush="True")

        for bug in data:
            if(bug['severity'] == 'serious'):
                sevs[0] += 1
            elif(bug['severity'] == 'grave'):
                sevs[1] += 1
            else:
                sevs[2] += 1

        print("done.")

        self.save_bug_data(sevs)

        return sevs

    def plot_bugs(self, start=0, end=time.time(), plot_type="overall", no_show=False):
        '''Generate a plot with bug history'''

        plot_title = "Release-Critical bugs in Testing"
        filename = self.plot_filename
        locator = mdt.DayLocator(bymonthday=range(1,16,15))
        date_format = "%m/%Y"

        if(end <= start and plot_type == None):
            end = time.time()
            start = 0

        if(plot_type == "freeze"):
            if(dt.datetime.now() > self.testing[1]):
                start = dt.datetime.timestamp(self.testing[1])
                plot_title += " since the Freeze"
            else:
                start = dt.datetime.timestamp(self.stable[1])
                plot_title += f" since the release of {self.stable[0]}"
            filename = self.freeze_filename
            
        plot_title += "\n" + dt.datetime.now().strftime("%a, %d.%m.%Y %H:%M:%S")

        tmp_data = Bugs.query.filter(Bugs.tstamp >= start, Bugs.tstamp <= end)

        query_data = []

        for row in tmp_data:
            t = []
            x = row.row()[0]
            t.append(dt.datetime.fromtimestamp(x))
            t.append(row.row()[1])
            t.append(row.row()[2])
            t.append(row.row()[3])
            query_data.append(t)

        # n_rows = len(query_data)
        data = np.array(query_data)

        del(tmp_data, query_data)

        max_bugs = int(np.amax(data[:, 1]))
        if(not start):
            min_date = np.amin(data, axis=0)[0] - dt.timedelta(days=7)
        else:
            min_date = dt.datetime.fromtimestamp(start) - dt.timedelta(days=7)
        if(not end):
            max_date = np.amax(data, axis=0)[0] + dt.timedelta(days=7)
        else:
            max_date = dt.datetime.fromtimestamp(end) + dt.timedelta(days=7)

        interval = max_date - min_date
        step = math.ceil((interval.days/8)/30)
        locator = mdt.MonthLocator(interval=step)

        # if(interval > dt.timedelta(weeks=32)):
        #     if(plot_type != "freeze"):
        #         locator = mdt.MonthLocator(interval=2)
        #     else:
        #         locator = mdt.MonthLocator()

        blurb = ""
        if(self.launch_date[0]):
            if(self.launch_date[1]):
                blurb = "Estimated launch date:\n"
            else:
                blurb = "TENTATIVE launch date:\n"
            blurb += self.launch_date[2].strftime("%Y-%m-%d")

        offset_dt = min_date + dt.timedelta(days=math.floor(interval.days*4/5))

        latest = self.get_latest()
        print(f"Creating plot (type: {plot_type})...")
        plt.figure(figsize=(12.8, 9.6))
        plt.rcParams.update({'font.size': 12})
        # Plot axes.
        plt.gca().xaxis.set_major_locator(locator)
        plt.gca().xaxis.set_major_formatter(mdt.DateFormatter(date_format))
        plt.xlabel('Date')
        plt.ylabel('# of bugs')
        plt.title(plot_title)
        plt.text(offset_dt, math.floor(max_bugs*3/4), blurb,
                ha = "center",
                bbox=dict(boxstyle="round",
                ec=(.8, .8, .8),
                fc=(1., 1., 1., .7)))
        plt.gca().set_xlim(min_date, max_date)
        plt.grid(True, which='major', axis='both')
        plt.plot(data[:,0], data[:, 1], '-b.',label="Serious")
        plt.plot(data[:,0], data[:, 2], '-r.',label="Grave")
        plt.plot(data[:,0], data[:, 3], '-g.',label="Critical")
        # if(fit == True):
        #     plot.plot(date, f_func(bugs[:,0]), color='red', label=fit_title)
        plt.legend()
        # print("Saving file " + plotimg_file)
        plt.savefig(self.image_prefix + filename)
        if(not no_show):
            print(f"Showing plot...")
            plt.show()
        else:
            print(f"Saving plot ({self.image_prefix + filename})...")
            self.save_plot(filename)

        return

    def save_plot(self, plotfile):
        ''' Save plot to database '''

        imgname = plotfile.split(".")[0]
        imgfile = open(self.image_prefix + plotfile, 'rb')
        blob = imgfile.read()

        img = Plot.query.filter_by(name=imgname).first()

        if(img != None):
            img.id = int(dt.datetime.timestamp(dt.datetime.now()))
            img.image = blob
            db.session.commit()
        else:
            new_img = Plot(
                id = int(dt.datetime.timestamp(dt.datetime.now())),
                name = imgname,
                image = blob
            )
            print("Committing plot to database...")
            db.session.add(new_img)
            db.session.commit()

        # try:
        print(f"Sending plot over FTP ({self.ftp_imgdir}/{imgname}.png)...")
        ftp = FTP(host=self.ftp_host, user=self.ftp_user, passwd = FTP_KEY)
        ftp.cwd(self.ftp_imgdir)
        imgfile.seek(0)      # Rewind the file or else!
        ftp.storbinary(f'STOR {imgname}.png', imgfile)
        imgfile.close()
        ftp.close()
        # except:
        #     print("Error in FTP upload! You should check it out.")
        #     pass

        return plotfile

    def get_plot(self, plotname):
        ''' Fetch plot from database '''

        img = Plot.query.filter_by(name=plotname).first()

        if(img != None):
            return img.image

        img = Plot.query.first()   # Needs improvement; flag for non-existing image?

        return img.image


    def create_tweet(self, sevs):
        """Create the text of the tweet you want to send."""

        # print("Composing tweet...", end="", flush="True")

        n_bugs = sum(sevs[1:])

        tweet = str(n_bugs) + " packages with RC bugs in Testing as of today"

        today = dt.datetime.today()

        # Has the freeze begun?
        t_days = today - self.testing[1]

        if(int(t_days.days) < 0):
            tweet += ". The Freeze has not begun yet."
            return tweet
        elif(int(t_days.days)%3):
            d = self.launch_date[2] - today
            if(d.days == 1):
                tweet += ". The Release is planned for tomorrow! Are you ready?"
            else:
                if(self.launch_date[0] == False):
                    tweet += ". There is no planned release date yet."
                else:
                    tweet +=  f". {str(d.days)} days until the "
                    if(self.launch_date[1] == True):
                        tweet += "planned "
                    else:
                        tweet += "TENTATIVE "
                    tweet += f"date for {self.testing[0]}'s release. Plan your party!"
        elif(int(t_days.days)%2):
            d = today - self.stable[1]
#            if(d.years > 0):
#                year = str(d.years) + " year " if d.years==1 else str(d.years) + " years "
#            if(d.months > 0):
#                month = str(d.months) + " month " if d.months==1 else str(d.months) + " months "
            year = ""
            month = ""
            day = str(d.days) + " day " if d.days==1 else str(d.days) + " days "
            tweet += " (" + year + month + day + "since the release of " + self.stable[0] + ")."
        else:
            d = today - self.testing[1]
            if(hasattr(d, "years") and d.years > 0):
                year = str(d.years) + " year " if d.years==1 else str(d.years) + " years "
            if(hasattr(d, "months") and d.months > 0):
                month = str(d.months) + " month " if d.months==1 else str(d.months) + " months "
            day = str(d.days) + " day " if d.days==1 else str(d.days) + " days "
            tweet += " (" + str(t_days.days) + str(self.day_th.get(d.days%10, "th")) + " day of the freeze)"

        print("done.")

        return tweet

    def tweet(self, sevs, mode):
        """Send out the text as a tweet."""

        # Mode defines if the tweet is sent or printet to output or both.
        # Mode = 1: tweet
        # Mode = 2: print out
        # Mode = 3: both
        # Mode = 0: none

        text = self.create_tweet(sevs)

        now = dt.datetime.now()
        ordinal = now.toordinal()
        if(ordinal%2):
            filename = self.plot_filename
        else:
            filename = self.freeze_filename

        img = self.get_plot(filename.split('.')[0])

        f = open("tmp.png", 'wb')
        f.write(img)
        f.close()

        # Twitter

        if(mode%2 == 1):
            # Twitter authentication
            print("Authenticating to Twitter...")
            auth = tweepy.OAuthHandler(C_KEY, C_SECRET)
            auth.set_access_token(A_TOKEN, A_TOKEN_SECRET)
            api = tweepy.API(auth)

            # Send the tweet and log success or failure
            try:
                if(mode%2 == 1):
                    print("Sending media...")
                    f = open("tmp.png", 'rb')
                    twitter_media_metadata = api.media_upload(filename, file=f)
                    f.close()
                    print("Sending tweet...")
                    twitter_update = api.update_status(text, media_ids=[twitter_media_metadata.media_id_string])
                else:
                    print("No message going out. Something wrong?")
            except tweepy.error.TweepError as e:
                print(e.reason)

        print("Tweeted: \"" + text + "\"")

        # Mastodon
        if(mode%2 == 1):
            print("Authenticating to Mastodon...")
            mast = mastodon.Mastodon(client_id = M_ID, client_secret = M_SECRET, access_token = M_TOKEN, api_base_url = self.mastodon_url)

            try:
                print("Sending media...")
                f = open("tmp.png", 'rb')
                media_metadata = mast.media_post(f, mime_type = 'image/png', file_name=filename)
                f.close()
                print("Sending toot...")
                mast_update = mast.status_post(text, media_ids=media_metadata.id)
            except mastodon.Mastodon.MastodonError as e:
                print(e.reason)

            print("Tooted: \"" + text + "\"")

        # Standard Output
        if(mode > 1):
            print("Message text: \"" + text + "\"")

        # Update log
        try:
            twit_id = twitter_update.id
        except:
            twit_id = 0
        try:
            mast_id = mast_update.id
        except:
            mast_id = 0

        new_log = Updates(
            tstamp = dt.datetime.timestamp(now),
            text = text,
            twitter_id = twit_id,
            mastodon_id = mast_id
        )

        db.session.add(new_log)
        db.session.commit()

        return
