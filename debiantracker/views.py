#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright (c) 2019-2021 Francisco M Neto
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from flask import render_template
from debiantracker import dt
from debiantracker.main import DebianTracker

@dt.route("/")
def index():
    return render_template("plot.html", plot_filename="bugs.png")

@dt.route("/image")
def default_image():
    bot = DebianTracker()
    image = bot.get_plot(plotname="bugs")
    if(image != False):
        return dt.response_class(image, mimetype="image/png")
    return

@dt.route("/image/<string:name>")
def image(name):
    bot = DebianTracker()
    if(not name):
        name = "bugs"
    image = bot.get_plot(plotname=name)
    if(image != False):
        return dt.response_class(image, mimetype="image/png")
    return
