# Copyright (c) 2019-2021 Francisco M Neto
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from debiantracker import db
from datetime import datetime as dt

class Bugs(db.Model):
    tstamp = db.Column(db.Integer, primary_key=True, nullable=False)
    serious = db.Column(db.Integer)
    grave = db.Column(db.Integer)
    critical = db.Column(db.Integer)

    def __str__(self):
        return f"Timestamp: {self.tstamp}; Serious: {self.serious}, Grave: {self.grave}, Critical: {self.critical}"

    def row(self):
        return [self.tstamp, self.serious, self.grave, self.critical]

class Plot(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    image = db.Column(db.LargeBinary)

    def __str__(self):
        return f"Name: {self.name}"

class Updates(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    tstamp = db.Column(db.Integer, primary_key=True, nullable=False)
    text = db.Column(db.String)
    twitter_id = db.Column(db.String)
    mastodon_id = db.Column(db.String)

    def __str__(self):
        time = dt.fromtimestamp(self.tstamp)
        ret = f"Update text: \"{self.text}\"\nTimestamp: {time}\nTwitter ID: {self.twitter_id}\nMastodon ID: {self.mastodon_id}"

