#!/usr/bin/python3
#

from apscheduler.schedulers.blocking import BlockingScheduler
import time
import json

import debiantracker as dt

def update_bug_data():
    bot = dt.DebianTracker()
    data = bot.fetch_bug_data()

    if(data != None):
        sevs = bot.process_bug_sevs(data)
        bot.plot_bugs()

    del(bot)

    return sevs

def send_tweet():

    bot = dt.DebianTracker()
    sevs = bot.get_latest()

    text = bot.create_tweet(sevs)
    bot.tweet(sevs, 2)

    del(bot)

if __name__ == "__main__":
    sched = BlockingScheduler()

    sched.add_executor('processpool')
    sched.add_job(update_bug_data, 'interval', hours=6)
    sched.add_job(send_tweet, 'cron', hour=12)
    # sched.add_job(update_auction_database, 'interval', hours=2)

    try:
        sched.start()
    except (KeyboardInterrupt,SystemExit):
        pass
