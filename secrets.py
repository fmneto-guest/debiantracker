import os

C_KEY = os.environ['C_KEY']
C_SECRET = os.environ['C_SECRET']
A_TOKEN = os.environ['A_TOKEN']
A_TOKEN_SECRET = os.environ['A_TOKEN_SECRET']
M_ID = os.environ['MASTODON_ID']
M_SECRET = os.environ['MASTODON_SECRET']
M_TOKEN = os.environ['MASTODON_TOKEN']
FTP_KEY = os.environ['FTP_KEY']
MYSQL_URI = os.environ['DATABASE_URI']
