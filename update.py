#!/usr/bin/python3
#

import time
import json
import os

from debiantracker import main as dt

bot = dt.DebianTracker()
data = bot.fetch_bug_data()

os.chdir("debiantracker")

if(data != None):
    sevs = bot.process_bug_sevs(data)
    bot.plot_bugs(no_show=True)
    bot.plot_bugs(plot_type='freeze', no_show=True)

del(bot)
